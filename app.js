require("dotenv").config();

const express = require("express");
const app = express();
const userRouter = require("./api/user/user.router");
const bodyParser = require("body-parser");
const cors = require("cors");




app.use(express.json());
app.use(cors(), bodyParser.urlencoded({ extended: true }), bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api/users", userRouter);





app.listen(process.env.APP_PORT, () => {
  console.log(`serveur démarré sur le port : ${process.env.APP_PORT}`);
});
