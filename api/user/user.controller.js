const {
  create,
  getAllUsers,
  getUserById,
  getUserByEmail,
  updateUser,
  deleteUser,
} = require("./user.service");
const { genSaltSync, hashSync, compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");

module.exports = {
  register: (req, res) => {
    const body = req.body;

    getUserByEmail(body.email, (err, results) => {
      if (err) {
        return;
      }
      if (results) {
        return res.json({
          success: 0,
          message: "Cette adresse email existe déjà",
        });
      } else {
        if (body.firstname && body.lastname && body.password && body.email) {
          const salt = genSaltSync(10);
          body.password = hashSync(body.password, salt);
          create(body, (err, results) => {
            if (err) {
              console.log(err);
              return res.status(500).json({
                success: 0,
                message: "Echec de l'inscription",
              });
            }
            return res.status(200).json({
              success: 1,
              data: results,
            });
          });
        } else {
          return res.status(500).json({
            success: 0,
            message:"champs requis",
          });
        }
      }
    });
  },

  findUserById: (req, res) => {
    const id = req.params.id;
    getUserById(id, (err, results) => {
      if (err) {
        return;
      }
      if (!results) {
        return res.json({
          success: 0,
          message: "L'utilisateur est introuvable",
        });
      }
      return res.json({
        success: 1,
        data: results,
      });
    });
  },
  findAllUsers: (req, res) => {
    getAllUsers((err, results) => {
      if (err) {
        return;
      }
      return res.json({
        success: 1,
        data: results,
      });
    });
  },
  updated: (req, res) => {
    const body = req.body;
    const salt = genSaltSync(10);
    body.password = hashSync(body.password, salt);
    updateUser(body, (err, results) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "La modification n'a pas pu etre effectuée",
        });
      }
      return res.status(200).json({
        success: 1,
        message: "La modification effectuée avec succès",
      });
    });
  },
  login: (req, res) => {
    const body = req.body;
    getUserByEmail(body.email, (err, results) => {
      if (err) {
        return;
      }
      if (!results) {
        return res.json({
          success: 0,
          message: "login et/ou mot de passe incorrect",
        });
      }
      const result = compareSync(body.password, results.password);
      if (result) {
        results.password = undefined;
        const jsontoken = sign({ result: results }, process.env.SECRET_KEY, {
          expiresIn: "1h",
        });
        return res.json({
          success: 1,
          message: "Authentification réussite",
          token: jsontoken,
        });
      } else {
        return res.json({
          success: 0,
          message: "Echec d'authentification",
        });
      }
    });
  },
  deleted: (req, res) => {
    const id = req.params.id;
    deleteUser(id, (err, results) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: "La supression n'a pas pu etre effectuée",
        });
      }
      return res.status(200).json({
        success: 1,
        message: "la suppression e été effectuée avec succès",
      });
    });
  },
};
