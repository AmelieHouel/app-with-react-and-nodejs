const {
  register,
  findAllUsers,
  findUserById,
  updated,
  login,
  deleted,
} = require("./user.controller");

const cors = require('cors');
const corsOptions = {
  origin: 'http://localhost:3000'
};

const { checkToken } = require("../auth/token.validation");

const router = require("express").Router();

//public
router.post("/", cors(corsOptions), register);
router.post("/login", login);

//private
router.get("/", checkToken, findAllUsers);
router.get("/:id", checkToken, findUserById);
router.patch("/", checkToken, updated);
router.delete("/:id", checkToken, deleted);

module.exports = router;
